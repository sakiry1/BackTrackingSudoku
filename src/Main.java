public class Main {
    public static void main(String[] args) {
        int[][] tabla = {
                {3, 0, 6, 5, 0, 8, 4, 0, 0},
                {5, 2, 0, 0, 0, 0, 0, 0, 0},
                {0, 8, 7, 0, 0, 0, 0, 3, 1},
                {0, 0, 3, 0, 1, 0, 0, 8, 0},
                {9, 0, 0, 8, 6, 3, 0, 0, 5},
                {0, 5, 0, 0, 9, 0, 6, 0, 0},
                {1, 3, 0, 0, 0, 0, 2, 5, 0},
                {0, 0, 0, 0, 0, 0, 0, 7, 4},
                {0, 0, 5, 2, 0, 6, 3, 0, 0}};

        if (buscarSolucion(tabla)) {
            printTablero(tabla);
        }
    }

    public static boolean buscarSolucion(int[][] tabla) {
        int xfila = 0;
        int ycol = 0;
        boolean aux = true; //referencia el return
        int valor= 1;

        if (anyCeldaVacia(tabla)) { //si la tabla esta con todos sus valores se acabo la busqueda
            return aux;
        }
        //recorrer la tabla por fila y col
        for (int i = 0; i < tabla.length; i++) {
            for (int j = 0; j < tabla.length; j++) {
                if (celdaVacia(tabla, i, j)) {//si la celda es cero (vacia) guarda su posicion
                    xfila = i;
                    ycol = j;
                    while (valor<10){ //busca q valor es aceptado en la casilla 0
                       if (numeroAceptado(tabla, xfila,ycol, valor)) {
                           tabla[xfila][ycol] = valor;
                           if (buscarSolucion(tabla))
                               return aux;
                           else
                               tabla[xfila][ycol] = 0;
                       }
                       valor++; //prueba sgt valor
                    }
                    aux= false; // quiere decir q no hay solucion por ese camino
                }
            }
        }
        return aux;
    }
    public static boolean checkValores(int[][] tabla, int fila, int col) {
        int valor = 1;
        while (valor<10) {
            if (numeroAceptado(tabla, fila, col, valor)) {
                tabla[fila][col]= valor;
                valor=10;
                return true;
            }
        }
        return false;
    }

    /**
     * celdaVacia mira si la celda es cero
     *
     * @param tabla es la tabla de juego
     * @return true si la celda es cero, false si ya tiene un valor
     */
    private static boolean celdaVacia(int[][] tabla, int fila, int col) {
        if (tabla[fila][col] == 0) { //si esta vacio
            return true;
        }
        return false;
    }

    /**
     * anyCeldaVacia me dice si la tabla esta completa o no
     *
     * @param tabla
     * @return true si esta completa or false cuando hay espacios con cero
     */
    private static boolean anyCeldaVacia(int[][] tabla) {
        for (int i = 0; i < tabla.length; i++) {
            for (int j = 0; j < tabla[i].length; j++) {
                if (tabla[i][j] == 0) {
                    int xFila = i;
                    int yCol = j;
                    return false;
                }
            }
        }
        return true;
    }

        /**
         * impresionMomentaria pinta la tabla al finalizar
         * @param tabla
         */
        private static void printTablero ( int[][] tabla){
            for (int i = 0; i < tabla.length; i++) {
                for (int j = 0; j < tabla[i].length; j++) {
                    System.out.print(" " + tabla[i][j] + " ");
                }
                System.out.println();
            }
        }

        /**
         * numeroAceptado
         * comprueba que valors cumpla con los requisitos de la tabla, se envia la posicion del valor con fila y columna
         * valor q cumpla fila, col y su sector
         *
         * @param tabla  del juego
         * @param fila   posicion del valor vacio
         * @param col    posicion del valor vacio
         * @param valors se pasa el valor a comprobar del 1 - 9
         * @return numAceptadoSector as boolean, true si cumple los requisitos, false cuando falla uno de los requisitos
         */
        private static boolean numeroAceptado ( int[][] tabla, int fila, int col, int valors){
            return revisaFila(tabla, fila, valors) && revisaCol(tabla, col, valors) && revisaSector(tabla, fila, col, valors);
        }

        /**
         *revisaSector , revisa un sector que esta entre los parametros de fila y columna 3 X 3
         * hace el mod3 para saber su fila y columna inicial de ese sector
         * @param tabla
         * @param fila
         * @param col
         * @param valor que tenemos que comparar
         * @return true si el valor es aceptado o false si ya se repite el valor
         */
        private static boolean revisaSector ( int[][] tabla, int fila, int col, int valor){
            int sectorFila = fila - fila % 3;//filaIni
            int sectorCol = col - col % 3;//colIni
            for (int i = 0; i < 3; i++) {
                for (int l = 0; l < 3; l++) {
                    if (tabla[sectorFila][sectorCol] == valor) { // si valor existe en el sector
                        return false;
                    }
                    sectorCol++;
                }
                sectorFila++;
                sectorCol = col - col % 3;
            }
            return true;
        }

        /**
         * revisaCol , revisa la columna de la tabla
         * @param tabla del sudoku
         * @param col que sera fija
         * @param valor numero que tenemos que comparar
         * @return true si el valor es aceptado o false si ya se repite el valor
         */
        private static boolean revisaCol ( int[][] tabla, int col, int valor){//fija Col
            for (int i = 0; i < tabla.length; i++) { //mientras recorra la fila con la col fijada
                if (tabla[i][col] == valor) {
                    return false;
                }
            }
            return true;
        }

        /**
         * revisaFila , revisa la fila de la tabla
         * @param tabla del sudoku
         * @param fila que sera fija
         * @param valor numero que tenemos que comparar
         * @return true si el valor es aceptado o false si ya se repite el valor
         */
        private static boolean revisaFila ( int[][] tabla, int fila, int valor){//fija Fila
            for (int i = 0; i < tabla.length; i++) { //mientras recorra la columna
                if (tabla[fila][i] == valor) {
                    return false;
                }
            }
            return true;
        }

    }

